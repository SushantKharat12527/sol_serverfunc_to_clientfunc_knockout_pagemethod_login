﻿using Newtonsoft.Json;
using Sol_LOGIN.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_LOGIN
{
    public partial class Default : System.Web.UI.Page
    {

      // public LoginDCDataContext _db = null;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public Default()
        {
             // _db = new LoginDCDataContext();
        }
        
    #region Web Api
    [WebMethod]
        public static int? CheckLoginApi(String userEntityJson)
        {
            int? status = 0;
            string message = null;
            LoginDCDataContext _db = new LoginDCDataContext();
            //bool status = false;
            try
            {
                
                if (userEntityJson != null)
                {

                    // Deserialize Json String into Object 
                    UserEntity userEntityObj = JsonConvert.DeserializeObject<UserEntity>(userEntityJson);

                    // Validate
                  
                    _db
                        ?.uspLoginCheck(
                            "CheckLogin"
                            , userEntityObj.UserName
                            , userEntityObj.Password
                            ,ref status
                            ,ref message
                            );
                    return status;
                }

                
            }
            catch (Exception)
            {
                throw;
            }

            return status;
        }
        #endregion 
    }
}