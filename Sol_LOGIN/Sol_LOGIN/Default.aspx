﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Sol_LOGIN.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
       <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css" />

    <style>
        body{
            padding : 10px;
            margin:auto;
        }

        .divCenterPosition{
            margin:0px auto;
            margin-top:150px;
        }

        .errorPanelHide{
            display:none;
        }
    </style>

    
    <script type="text/javascript" src="Scripts/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="Scripts/knockout-3.4.2.js"></script>


    <script type="text/javascript">
        function pageLoad() {

            $(document).ready(function () {

                //alert("Dom is Ready");

                loginViewModel();

               

            });

            function loginViewModel()
            {
                var model = {
                    userName: ko.observable(), // Get and Set value 
                    password: ko.observable(), // Get and Set Value 

                    loginClick: function () {
                        //alert("Knokout Click event Call");

                        // convert UI data into Json
                        var loginJson = ko.toJSON({
                            UserName: model.userName(),
                            Password: model.password()
                        });

                        alert(loginJson);

                        // call Login Check Api
                        PageMethods.CheckLoginApi(loginJson, function (result) {

                            alert("Api Call");

                            alert(result);

                            if (result == 1)
                            {
                                // Show Message 
                                $("#lblMessage").text("Login Succesfully");
                                // Show Panel
                                $("#divMessagePanel")
                                    .show()
                                    .fadeOut(3000, function () {
                                        // Redirect to Welcome Page
                                    window.location.href = "Welcome.aspx";

                                });
                            }
                            else
                            {
                                  // Show Message 
                                $("#lblMessage").text("Login Failed");
                                // Show Panel
                                $("#divMessagePanel")
                                    .show()
                                    .fadeOut(3000, function () {

                                    // Clear Text Box Value and Set focus on User Name TextBox
                                    $("#txtUserName").val("");
                                    $("#txtPassword").val("");

                                    $("txtUserName").focus();

                                });
                            }

                        });


                       
                    }
                }

                // For Testing Purpose Only 
                //$("#btnSubmit").click(function () {

                //    //alert("Button Click");

                //    var jsonString = JSON.stringify({ UserName: model.userName(), Password: model.password() });
                //    alert(jsonString);

                //});

                ko.applyBindings(model);
            }

        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
    
             <asp:ScriptManager ID="scriptManager" runat="server" EnablePageMethods="true"></asp:ScriptManager>

        <asp:UpdatePanel ID="updatePanel" runat="server">
                <ContentTemplate>

                    <div class="w3-card-4 divCenterPosition" style="width: 40%">
                        <div class="w3-container w3-teal">
                            <h2>Login</h2>
                        </div>

                        <div class="w3-container w3-white">

                            <table class="w3-table">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblUserName" runat="server" AccessKey="U" AssociatedControlID="txtUserName" CssClass="w3-text-teal">
                                    <u>U</u>serName
                                        </asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtUserName" runat="server" CssClass="w3-input w3-border w3-light-grey" data-bind="value: userName"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblPassword" runat="server" AccessKey="P" AssociatedControlID="txtPassword" CssClass="w3-text-teal">
                                    <u>P</u>assword
                                        </asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" CssClass="w3-input w3-border w3-light-grey" data-bind="value: password"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button ID="btnSubmit" runat="server" AccessKey="N" Text="Sign In" CssClass="w3-btn w3-blue-grey" data-bind="click: loginClick"></asp:Button>
                                    </td>
                                </tr>
                                 <tr>
                                    <td>
                                        <div id="divMessagePanel" class="w3-panel w3-blue w3-card-4 errorPanelHide">
                                            <span id="lblMessage"></span>
                                        </div>
                                    </td>
                                </tr>
                            </table>

                        </div>

                    </div>

                </ContentTemplate>
            </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
